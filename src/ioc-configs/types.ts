export const TYPES = {
    //Voucher
    VoucherController: Symbol.for("VoucherController"),
    VoucherRepository: Symbol.for("VoucherRepository"),
    VoucherService: Symbol.for("VoucherService"),

    //Purchase Transaction
    PurchaseTransactionRepository: Symbol.for("PurchaseTransactionRepository"),
    PurchaseTransactionService: Symbol.for("PurchaseTransactionService"),

    //Customer
    CustomerController: Symbol.for("CustomerController"),
    CustomerService: Symbol.for("CustomerService"),
    CustomerRepository: Symbol.for("CustomerRepository"),

    //Customer Voucher
    CustomerVoucherService: Symbol.for("CustomerVoucherService"),
    CustomerVoucherRepository: Symbol.for("CustomerVoucherRepository"),

    //Partner
    FaceRecognitionPartnerService: Symbol.for("FaceRecognitionPartnerService"),

};