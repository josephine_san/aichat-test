export class PaginationHelper {
 
    public getPagination(page: number, size: number): any {
        const limit = size ? +size : 10;
        const offset = page ? page * limit : 0;
        return {limit, offset}
    }

    public getPagingData(dataInput: any, page: number, limit: number) : any {
      const {count: totalItems, rows: data } =  dataInput
      const currentPage = page ? +page : 0;
      const totalPages = Math.ceil(totalItems / limit);
      return { totalItems, data, totalPages, currentPage };
    }
}