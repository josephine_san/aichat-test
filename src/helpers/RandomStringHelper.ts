export class RandomStringHelper {
  private static instance: RandomStringHelper

  public static getInstance(): RandomStringHelper {
    if (RandomStringHelper.instance === null || RandomStringHelper.instance === undefined) {
      RandomStringHelper.instance = new RandomStringHelper();
    }
    return RandomStringHelper.instance;
  }

  public makeRandom(length: number): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }
    return result;
  }
}