import 'moment-timezone';
import * as moment from 'moment';

export class DateHelper {
	private static instance: DateHelper;

	public static getInstance(): DateHelper {
		if (DateHelper.instance == null) {
			DateHelper.instance = new DateHelper();
		}

		return DateHelper.instance;
	}

	/**
	 * returning current datetime on milliseconds
	 */
	public getCurrentTimeOnMillis(): number {
		const date = new Date();
		const jakartaTimezone = moment(date).tz("Asia/Jakarta");
		const timeInMillis = jakartaTimezone.format("x");
		return Number(timeInMillis);
	}

	/**
	 * returning current datetime with second addition on milliseconds
	 */
	public getCurrentTimeOnAddedBySecond(_second: number = 0): number {
		const jakartaTimezone = moment().tz("Asia/Jakarta");
		const timeInMillis = jakartaTimezone.add(_second, "seconds").format("x");
		return Number(timeInMillis);
	}

	/**
	 * returning current datetime subtracted with N hour on milliseconds
	 * @param _hour : number of subtracted hour
	 */
	public getCurrentTimeSubtractByHour(_hour: number = 0): number {
		const jakartaTimezone = moment().tz("Asia/Jakarta");
		const timeInMillis = jakartaTimezone.subtract(_hour, "hours").format("x");
		return Number(timeInMillis);
	}

		/**
	 * returning current datetime subtracted with N hour on milliseconds
	 * @param days : number of subtracted hour
	 */
		 public getCurrentTimeSubtractByDay(_day: number = 0): number {
			const date = this.getStartOfDayInMillis();
			const jakartaTimezone = moment(date).tz("Asia/Jakarta");
			const timeInMillis = jakartaTimezone.subtract(_day, "days").format("x");
			return Number(timeInMillis);
		}

	/**
	 * returning standard datetime string to milliseconds
	 * @param _stringDate : string of date time in string
	 */
	public getStringDateToMillis(_stringDate: string): number {
		const date = new Date(_stringDate); // some mock date
		const milliseconds = date.getTime();
		return Number(milliseconds);
	}

	/**
	 * returning standard datetime string to end of current date in milliseconds
	 * @param _stringDate : string of date time in string
	 */
	public getStringDateToEndOfDayInMillis(_stringDate: string): number {
		const timeInMillis = moment(_stringDate).endOf("day").format("x");
		return Number(timeInMillis);
	}

	/**
	 * returning standard datetime string to start of current date in milliseconds
	 * @param _stringDate : string of date time in string
	 */
	public getStringDateToStartOfDayInMillis(_stringDate: string): number {
		const timeInMillis = moment(_stringDate).startOf("day").format("x");
		return Number(timeInMillis);
	}

	/**
	 * returning start of current date in milliseconds
	 */
	public getStartOfDayInMillis(): number {
		const jakartaTimezone = moment().tz("Asia/Jakarta");
		const timeInMillis = jakartaTimezone.startOf("day").format("x");;
		return Number(timeInMillis);
	}

	/**
	 * returning end of current date in milliseconds
	 */
	public getEndOfDayInMillis(): number {
		const jakartaTimezone = moment().tz("Asia/Jakarta");
		const timeInMillis = jakartaTimezone.endOf("day").format("x");
		return Number(timeInMillis);
	}

	/**
	 * return Date format
	 */
	public millisToDateISO(_millis: number): string {
		return moment(_millis).format("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	}

	/**
	 * returning current datetime to ISO 8610 format on string
	 */
	public getCurrentISO8601DateTimeString(): string {
		const jakartaTimezone = moment().tz("Asia/Jakarta");
		return jakartaTimezone.toISOString()
	}

}
