INSERT INTO ai_test.customers(first_name, last_name, gender, date_of_birth, contact_number, email) values ("Josephine", "Santika", "Female", STR_TO_DATE('1-01-1999', '%d-%m-%Y'), "081111222333", "josephine@gmail.com"),
("Jerry", "Anderson", "Male", STR_TO_DATE('12-05-2005', '%d-%m-%Y'), "082241424124", "jerryanderson@gmail.com"),
("Alian", "Space", "Male", STR_TO_DATE('11-02-2000', '%d-%m-%Y'), "081235454322", "alianspace@gmail.com"),
("Christianti", "Wahyuni", "Female", STR_TO_DATE('19-10-2005', '%d-%m-%Y'), "082213234234", "christiantiwahyuni@gmail.com"),
("Joseph", "Christian", "Male", STR_TO_DATE('12-05-1998', '%d-%m-%Y'), "082234343143", "josephchristian@gmail.com")