INSERT INTO ai_test.purchase_transactions(customer_id, total_spent, total_saving, transaction_at) values (1, 700000, 0 , STR_TO_DATE('31-12-2021', '%d-%m-%Y')),
(1, 600000, 0 , STR_TO_DATE('21-01-2022', '%d-%m-%Y')), (1, 500000, 0 , STR_TO_DATE('23-01-2022', '%d-%m-%Y')),
(2, 500000, 0 , STR_TO_DATE('22-01-2022', '%d-%m-%Y')), (2, 700000, 0 , STR_TO_DATE('22-01-2022', '%d-%m-%Y')),
(3, 700000, 0 , STR_TO_DATE('22-01-2022', '%d-%m-%Y')), (3, 700000, 0 , STR_TO_DATE('22-01-2022', '%d-%m-%Y')),
(3, 400000, 0 , STR_TO_DATE('20-01-2022', '%d-%m-%Y')), (4, 700000, 0 , STR_TO_DATE('12-01-2022', '%d-%m-%Y'))