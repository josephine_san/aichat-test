import { injectable } from "inversify";

@injectable()
export class FaceRecognitionPartnerService{

  constructor(
    
  ) { }

  async imageRecognitionAPI(_file: Buffer): Promise<boolean> {
    return true;
  }
}