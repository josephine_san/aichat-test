import { AppConfig, ApiLambdaApp } from "ts-lambda-api";
import * as path from "path";
import { Sequelize } from 'sequelize';
import { Context, APIGatewayEvent } from "aws-lambda"
import { TYPES } from "./ioc-configs/types";
import { VoucherController } from "./controllers/VoucherController";
import { VoucherService } from "./services/VoucherService";
import { VoucherRepositoryImpl } from "./repositories/impl/VoucherRepositoryImpl";
import { VoucherDefineModel } from "./models/VoucherModel";
import { CustomerController } from "./controllers/CustomerController";
import { CustomerService } from "./services/CustomerService";
import { PurchaseTransactionService } from "./services/PurchaseTransactionService";
import { CustomerRepositoryImpl } from "./repositories/impl/CustomerRepositoryImpl";
import { PurchaseTransactionRepositoryImpl } from "./repositories/impl/PurchaseTransactionRepositoryImpl";
import { CustomerDefineModel } from "./models/CustomerModel";
import { PurchaseTransactionDefineModel } from "./models/PurchaseTransactionModel";
import { CustomerVoucherDefineModel } from "./models/CustomerVoucherModel";
import { CustomerVoucherService } from "./services/CustomerVoucherService";
import { CustomerVoucherRepositoryImpl } from "./repositories/impl/CustomerVoucherRepositoryImpl";
import { FaceRecognitionPartnerService } from "./partner/FaceRecognitionPartnerService";

const appConfig = new AppConfig();

appConfig.base = "/api/v1";
appConfig.version = "v1";

const controllersPath = [path.join(__dirname, "controllers")];
const app = new ApiLambdaApp(controllersPath, appConfig);

const dbAiTest = {
  protocol: 'mysql',
  user: `${process.env.MYSQL_USERNAME}`,
  password: `${process.env.MYSQL_PASSWORD}`,
  host: `${process.env.MYSQL_HOST}`,
  port: process.env.MYSQL_PORT,
  name: `${process.env.MYSQL_DB}`
}

const sequelizeDBAiTest = new Sequelize(`${dbAiTest.protocol}://${dbAiTest.user}:${dbAiTest.password}@${dbAiTest.host}:${dbAiTest.port}/${dbAiTest.name}`);

export async function handler(event: APIGatewayEvent, context: Context) {
  // Initiate models
  VoucherDefineModel.getInstance(sequelizeDBAiTest);
  CustomerDefineModel.getInstance(sequelizeDBAiTest);
  PurchaseTransactionDefineModel.getInstance(sequelizeDBAiTest);
  CustomerVoucherDefineModel.getInstance(sequelizeDBAiTest);

  app.configureApp(container => {
    // CONTROLLER
		if (!container.isBound(TYPES.VoucherController)) {
			container.bind(TYPES.VoucherController).to(VoucherController);
		}
    if (!container.isBound(TYPES.CustomerController)) {
			container.bind(TYPES.CustomerController).to(CustomerController);
		}

    //SERVICE
    if (!container.isBound(TYPES.VoucherService)){
      container.bind(TYPES.VoucherService).to(VoucherService);
    }
    if (!container.isBound(TYPES.CustomerService)){
      container.bind(TYPES.CustomerService).to(CustomerService);
    }
    if (!container.isBound(TYPES.PurchaseTransactionService)){
      container.bind(TYPES.PurchaseTransactionService).to(PurchaseTransactionService);
    }
    if (!container.isBound(TYPES.CustomerVoucherService)){
      container.bind(TYPES.CustomerVoucherService).to(CustomerVoucherService);
    }

    //REPO
    if (!container.isBound(TYPES.VoucherRepository)){
      container.bind(TYPES.VoucherRepository).to(VoucherRepositoryImpl);
    }
    if (!container.isBound(TYPES.CustomerRepository)){
      container.bind(TYPES.CustomerRepository).to(CustomerRepositoryImpl);
    }
    if (!container.isBound(TYPES.PurchaseTransactionRepository)){
      container.bind(TYPES.PurchaseTransactionRepository).to(PurchaseTransactionRepositoryImpl);
    }
    if (!container.isBound(TYPES.CustomerVoucherRepository)){
      container.bind(TYPES.CustomerVoucherRepository).to(CustomerVoucherRepositoryImpl);
    }

    //Partner
    if (!container.isBound(TYPES.FaceRecognitionPartnerService)){
      container.bind(TYPES.FaceRecognitionPartnerService).to(FaceRecognitionPartnerService);
    }
  });
  return await app.run(event, context)
}