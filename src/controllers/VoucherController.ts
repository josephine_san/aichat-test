import { BaseController } from "./BaseController";
import { apiController, response, GET } from "ts-lambda-api";
import { Response } from "lambda-api";
import { injectable, inject } from "inversify";
import { RandomStringHelper } from "../helpers/RandomStringHelper";
import { VoucherModel } from "../models/VoucherModel";
import { TYPES } from "../ioc-configs/types";
import { VoucherService } from "../services/VoucherService";

@apiController("/voucher")
@injectable()
export class VoucherController extends BaseController {

  constructor(
    @inject(TYPES.VoucherService) private readonly voucherService: VoucherService,
  ) {
    super();
  }

  @GET("/generate")
  public async generateVoucher (@response response: Response) {
    try {
      for (let i=0; i< 1000; i++) {
        const generateVoucherCode = RandomStringHelper.getInstance().makeRandom(7);
        let _model: VoucherModel = {
          code: generateVoucherCode
        }
        console.log("[INFO] Voucher Model Request Body => ", _model);
        const result = await this.voucherService.createVoucher(_model);
      }

      this.respondSuccess(
				response,
				super.DEFAULT_STATUS_CODE,
				super.DEFAULT_STATUS_MESSAGE,
				(body) => {
					return body;
				}
			);
    }catch (err){
      console.log("Error => ", err);
      this.respondErrorGeneral(response, err.message);
    }
  }
}