import { BaseController } from "./BaseController";
import { apiController, POST, body, response, GET, queryParam, rawBody } from "ts-lambda-api";
import { Response } from "lambda-api";
import { injectable, inject } from "inversify";
import { TYPES } from "../ioc-configs/types";
import { CustomerService } from "../services/CustomerService";
import { VoucherService } from "../services/VoucherService";
import ErrorConstant from "../constant/ErrorConstant";
import { CustomerVoucherService } from "../services/CustomerVoucherService";
import { CustomerVoucherModel } from "../models/CustomerVoucherModel";
import { DateHelper } from "../helpers/DateHelper";
import { VoucherModel } from "../models/VoucherModel";

@apiController("/customer")
@injectable()
export class CustomerController extends BaseController {

  constructor(
    @inject(TYPES.CustomerService) private readonly customerService: CustomerService,
    @inject(TYPES.VoucherService) private readonly voucherService: VoucherService,
    @inject(TYPES.CustomerVoucherService) private readonly customerVoucherService: CustomerVoucherService
  ) {
    super();
  }

  @GET("/check/eligible")
  public async generateVoucher (
    @queryParam("customerPhone") customerPhone: string,
    @response response: Response) {
      try {
        const checkEligible = await this.customerService.checkEligibleCustomer(customerPhone);
        console.log("Customer is Eligible => ", checkEligible);
        if (checkEligible.isEligible === true) {
          const customerVoucherFound = await this.customerVoucherService.getCustomerVoucherByCUstomerId(checkEligible.customerId);
          if (customerVoucherFound) {
            throw new Error(ErrorConstant.VOUCHER_LIMIT_REACH_422);
          }
          const foundAllVoucher = await this.voucherService.getAllVoucher(0,0);
          if (foundAllVoucher.totalItems === 0) {
            throw new Error(ErrorConstant.VOUCHER_LIMIT_REACH_422);
          }

          const voucher = foundAllVoucher.data[0];
          const customerVoucher: CustomerVoucherModel = {
            customerId: checkEligible.customerId,
            voucherId: voucher.id,
            isUsed: false,
            isValid: false,
            startDate: DateHelper.getInstance().getCurrentTimeOnMillis(),
            endDate: DateHelper.getInstance().getCurrentTimeOnAddedBySecond(600)
          }

          const createdCustomerVoucher = await this.customerVoucherService.createCustomerVoucher(customerVoucher);
          if (createdCustomerVoucher) {
            const voucherUpdated: VoucherModel = {
              code: voucher.code,
              isLock: true
            }

            const updatedVoucher = await this.voucherService.updatedVoucher(voucher.id, voucherUpdated);
          }
        } else {
          throw new Error(ErrorConstant.NOT_ELIGIBLE_422);
        }

        this.respondSuccess(
          response,
          super.DEFAULT_STATUS_CODE,
          super.DEFAULT_STATUS_MESSAGE,
          (body) => {
            return body;
          }
        );
      }catch (err) {
        console.log("[ERROR - customer check eligible]:: ", err);
        this.respondErrorGeneral(response, err.message);
      }
  }

  @POST("/validate/photo")
  public async validatePhotoSubmission(
    @queryParam("customerPhone") customerPhone: string,
    @rawBody file: Buffer,
    @response response: Response) {
      try {
        const result = await this.customerService.validateSubmission(customerPhone, file);
        this.respondSuccess(
          response,
          super.DEFAULT_STATUS_CODE,
          super.DEFAULT_STATUS_MESSAGE,
          (body) => {
            body.data = result
            return body;
          }
        );
      }catch(err) {
        console.log("ERROR::", err);
        this.respondErrorGeneral(response, err.message)
      }
  }
}