import { injectable } from "inversify";
import { Response } from 'lambda-api';
import { TokenExpiredError } from "jsonwebtoken";
import HttpStatusCode from "../helpers/HttpStatusCode";
import ErrorConstant from "../constant/ErrorConstant";

export interface IResponsable {
	respondError(
		response: Response,
		statusCode: number,
		message: string,
		responseBuilder?: (body: any) => any,
	): any;

	respondSuccess(
		response: Response,
		statusCode: number,
		message: string,
		responseBuilder?: (body: any) => any,
	): any;

	respondErrorGeneral(
		response: Response,
		error: ErrorConstant
	): any;
}

@injectable()
export abstract class BaseController implements IResponsable {
	protected readonly DEFAULT_STATUS_CODE = 200;
	protected readonly DEFAULT_STATUS_MESSAGE = 'OK';
	protected readonly DEFAULT_ERROR_STATUS_CODE = 500;
	protected readonly DEFAULT_ERROR_STATUS_MESSAGE = 'Internal Server Error';

	constructor() {
	}

	respondError(
		response: Response,
		statusCode: number = this.DEFAULT_ERROR_STATUS_CODE,
		message: string = this.DEFAULT_ERROR_STATUS_MESSAGE,
		responseBuilder?: (body: any) => any,
	) {
		let body = {
			statusCode: statusCode,
			statusMessage: message,
			error: undefined,
		};
		body = responseBuilder(body);
		response
			.header('Access-Control-Allow-Origin', '*')
			.status(statusCode)
			.send(body);
	}

	respondSuccess(
		response: Response,
		statusCode: number = this.DEFAULT_STATUS_CODE,
		message: string = this.DEFAULT_STATUS_MESSAGE,
		responseBuilder: (body: any) => any,
	) {
		let body = {
			statusCode: statusCode,
			statusMessage: message,
			data: undefined,
		};

		body = responseBuilder(body);
		response
			.header('Access-Control-Allow-Origin', '*')
			.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE')
			.status(statusCode)
			.send(body);
	}

	respondSuccessPdf(
		response: Response,
		statusCode: number = this.DEFAULT_STATUS_CODE,
		body: String,
		fileName: String,
	) {
		response
			.header('Access-Control-Allow-Origin', '*')
			.header('Content-Type', 'text/pdf')
			.header(
				'Content-Disposition',
				`attachment; filename="${fileName}.pdf"`,
			)
			.status(statusCode)
			.send(body);
	}

	respondErrorGeneral(response: Response, error: string): any {
		let body = {
			statusCode: this.DEFAULT_ERROR_STATUS_CODE,
			statusMessage: this.DEFAULT_ERROR_STATUS_MESSAGE,
		};

		const errorArr = error.split('|');
		switch (errorArr[0]) {
			case '400':
				body.statusCode = HttpStatusCode.BAD_REQUEST;
				body.statusMessage = errorArr[1];
				break;
			case '401':
				body.statusCode = HttpStatusCode.UNAUTHORIZED;
				body.statusMessage = errorArr[1];
				break;
			case '404':
				body.statusCode = HttpStatusCode.NOT_FOUND;
				body.statusMessage = errorArr[1];
				break;
			case '422':
				body.statusCode = HttpStatusCode.UNPROCESSABLE_ENTITY;
				body.statusMessage = errorArr[1];
				break;
			case '402':
				body.statusCode = HttpStatusCode.UNPROCESSABLE_ENTITY;
				body.statusMessage = errorArr[1];
				break;
			default: //500
				body.statusCode = HttpStatusCode.INTERNAL_SERVER_ERROR;
				body.statusMessage = 'internal server error';
				break;
		}

		response
			.header('Access-Control-Allow-Origin', '*')
			.status(body.statusCode)
			.send(body);
	}


}