import { inject, injectable } from "inversify";
import { TYPES } from "../ioc-configs/types";
import { CustomerVoucherRepository } from "../repositories/CustomerVoucherRepository";
import { CustomerVoucherModel } from "../models/CustomerVoucherModel";

@injectable()
export class CustomerVoucherService{

  constructor(
    @inject(TYPES.CustomerVoucherRepository) private readonly customerVoucherRepo: CustomerVoucherRepository 
  ) { }

  createCustomerVoucher(_model: CustomerVoucherModel): Promise<CustomerVoucherModel> {
    return this.customerVoucherRepo.createCustomerVoucherModel(_model);
  }

  getCustomerVoucherByCUstomerId(_customerId: number): Promise<CustomerVoucherModel> {
    return this.customerVoucherRepo.getCustomerVoucherByCustomerId(_customerId);
  }

  getCustomerVoucherWithinDate(_customerId: number, _date: number): Promise<CustomerVoucherModel> {
    return this.customerVoucherRepo.getCustomerVoucherWithinDate(_customerId, _date);
  }

  updatedCustVoucher(_id: number, _model: CustomerVoucherModel): Promise<CustomerVoucherModel> {
    return this.customerVoucherRepo.updatedCustomerVoucher(_id, _model);
  }

  destroy(customerId: number, voucherId: number): Promise<CustomerVoucherModel> {
    return this.customerVoucherRepo.destroy(customerId, voucherId);
  }
}