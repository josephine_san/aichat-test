import { inject, injectable } from "inversify";
import { TYPES } from "../ioc-configs/types";
import { CustomerRepository } from "../repositories/CustomerRepository";
import ErrorConstant from "../constant/ErrorConstant";
import { PurchaseTransactionService } from "./PurchaseTransactionService";
import { CustomerVoucherService } from "./CustomerVoucherService";
import { FaceRecognitionPartnerService } from "../partner/FaceRecognitionPartnerService";
import { DateHelper } from "../helpers/DateHelper";
import { CustomerVoucherModel } from "../models/CustomerVoucherModel";
import { VoucherService } from "./VoucherService";
import { VoucherModel } from "../models/VoucherModel";
const dolarToRp = 15000;

@injectable()
export class CustomerService {

  constructor(
    @inject(TYPES.CustomerRepository) private readonly customerRepo: CustomerRepository,
    @inject(TYPES.PurchaseTransactionService) private readonly purchaseTransactionService: PurchaseTransactionService,
    @inject(TYPES.CustomerVoucherService) private readonly customerVoucherService: CustomerVoucherService,
    @inject(TYPES.FaceRecognitionPartnerService) private readonly faceRecognitionService: FaceRecognitionPartnerService,
    @inject(TYPES.VoucherService) private readonly voucherService: VoucherService

  ) { }

  async checkEligibleCustomer(_phone: string): Promise<any> {
    const foundCustomer = await this.customerRepo.getCustomerByPhone(_phone);
    if (!foundCustomer) {
      throw new Error(ErrorConstant.NOT_FOUND_404 + "[Customer]")
    }

    const customerUsedVoucher = await this.customerVoucherService.getCustomerVoucherByCUstomerId(foundCustomer.id);
    if (customerUsedVoucher) {
      throw new Error(ErrorConstant.VOUCHER_LIMIT_REACH_422);
    }

    const getAllTransaction = await this.purchaseTransactionService.getAllPurchaseTransaction(foundCustomer.id, -1, 0);
    console.log("All Transaction => ", getAllTransaction);
    if (getAllTransaction.totalItems < 3) {
      return { customerId: foundCustomer.id, isEligible: false };
    }

    let totalSpent = 0, totalSaving = 0;
    getAllTransaction.data.forEach(transaction => {
      totalSpent += transaction.total_spent;
      totalSaving += transaction.total_saving;
    })

    totalSpent = totalSpent - totalSaving;
    if (totalSpent < dolarToRp * 100) {
      return { customerId: foundCustomer.id, isEligible: false }
    }
    return { customerId: foundCustomer.id, isEligible: true };
  }

  async validateSubmission(_customerPhone: string, _file: Buffer): Promise<any> {
    const foundCustomer = await this.customerRepo.getCustomerByPhone(_customerPhone);
    if (!foundCustomer) {
      throw new Error(ErrorConstant.NOT_FOUND_404 + "[Customer]")
    }

    const customerVoucher = await this.customerVoucherService.getCustomerVoucherByCUstomerId(foundCustomer.id);
    if (!customerVoucher) {
      throw new Error(ErrorConstant.NOT_FOUND_404 + "[Voucher]");
    }

    if (customerVoucher.isValid === true)
      throw new Error(ErrorConstant.VOUCHER_LIMIT_REACH_422);

    const checkRecognition = await this.faceRecognitionService.imageRecognitionAPI(_file);
    if (checkRecognition === true) {
      const date = DateHelper.getInstance().getCurrentTimeOnMillis();
      const foundCustomerVoucher = await this.customerVoucherService.getCustomerVoucherWithinDate(foundCustomer.id, date);
      if (foundCustomerVoucher) {
        const voucherCustModel: CustomerVoucherModel = {
          isValid: true
        }
        const updatedVoucherCustomer = await this.customerVoucherService.updatedCustVoucher(foundCustomerVoucher.id, voucherCustModel);
        const foundVoucher = await this.voucherService.getVoucherById(foundCustomerVoucher.voucherId);
        return { voucherCode: foundVoucher.code }
      }
    }
    if (customerVoucher) {
      const deletedVoucher = await this.customerVoucherService.destroy(foundCustomer.id, customerVoucher.voucherId);
      const updatedVoucherModel: VoucherModel = {
        isLock: false
      }
      const updatedVoucher = await this.voucherService.updatedVoucher(customerVoucher.voucherId, updatedVoucherModel);
    }
    return { voucherCode: null }
  }
}