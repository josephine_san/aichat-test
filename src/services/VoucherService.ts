import { inject, injectable } from "inversify";
import { VoucherRepository } from "../repositories/VoucherRepository";
import { TYPES } from "../ioc-configs/types";
import { VoucherModel } from "../models/VoucherModel";

@injectable()
export class VoucherService{

  constructor(
    @inject(TYPES.VoucherRepository) private readonly voucherRepo: VoucherRepository,
  ) { }

  createVoucher(_model: VoucherModel): Promise<VoucherModel> {
    return this.voucherRepo.createVoucher(_model);
  }

  getAllVoucher(pageNumber: number, pageSize: number): Promise<any> {
    return this.voucherRepo.getAllVoucher(pageNumber, pageSize);
  }

  updatedVoucher(_id: number, _model: VoucherModel): Promise<VoucherModel> {
    return this.voucherRepo.updateVoucher(_id, _model);
  }

  getVoucherById(_id: number): Promise<VoucherModel> {
    return this.voucherRepo.getVoucherById(_id);
  }
}