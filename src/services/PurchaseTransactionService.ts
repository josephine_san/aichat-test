import { inject, injectable } from "inversify";
import { TYPES } from "../ioc-configs/types";
import { PurchaseTransactionRepository } from "../repositories/PurchaseTransactionRepository";

@injectable()
export class PurchaseTransactionService{

  constructor(
    @inject(TYPES.PurchaseTransactionRepository) private readonly purchaseTransactionRepo: PurchaseTransactionRepository,
  ) { }

  getAllPurchaseTransaction(customer_id: number, pageNumber: number, pageSize: number): Promise<any> {
    return this.purchaseTransactionRepo.getAllTransactionWithin30Days(customer_id, pageNumber, pageSize);
  }
}