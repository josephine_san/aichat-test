import { VoucherModel } from "../models/VoucherModel";

export interface VoucherRepository {
  createVoucher(_model: VoucherModel): Promise<VoucherModel>;
  getAllVoucher(pageNumber: number, pageSize: number): Promise<any>;
  updateVoucher(_id: number, _model: VoucherModel): Promise<VoucherModel>;
  getVoucherById(_id: number): Promise<VoucherModel>;
}