import { CustomerModel } from "../models/CustomerModel";

export interface CustomerRepository{
  getCustomerByPhone(_phone: string): Promise<CustomerModel>;
}