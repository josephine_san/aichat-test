export interface PurchaseTransactionRepository {
  getAllTransactionWithin30Days(customer_id: number, pageNumber: number, pageSize: number): Promise<any>;
}