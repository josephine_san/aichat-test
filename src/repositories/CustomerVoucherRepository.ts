import { CustomerVoucherModel } from "../models/CustomerVoucherModel";

export interface CustomerVoucherRepository {
  getCustomerVoucherByCustomerId(_customerId: number): Promise<CustomerVoucherModel>;
  createCustomerVoucherModel(_model: CustomerVoucherModel): Promise<CustomerVoucherModel>;
  getCustomerVoucherWithinDate(_customerId: number, _date: number): Promise<CustomerVoucherModel>;
  updatedCustomerVoucher(_id: number, _model: CustomerVoucherModel): Promise<CustomerVoucherModel>;
  destroy(customerId: number, voucherId: number): Promise<CustomerVoucherModel>;
}