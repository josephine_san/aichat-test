import { injectable } from "inversify";
import { Op } from "sequelize";
import { PurchaseTransactionDefineModel } from "../../models/PurchaseTransactionModel";
import { PaginationHelper } from "../../helpers/PaginationHelper";
import { PurchaseTransactionRepository } from "../PurchaseTransactionRepository";
import { DateHelper } from "../../helpers/DateHelper";

@injectable()
export class PurchaseTransactionRepositoryImpl implements PurchaseTransactionRepository {

  paginationHelper: PaginationHelper;
  constructor() {
    this.paginationHelper = new PaginationHelper();
  }

  async getAllTransactionWithin30Days(customer_id: number, pageNumber: number, pageSize: number): Promise<any> {
    const startDate = DateHelper.getInstance().getCurrentTimeSubtractByDay(30);
    console.log("Start Date => ", startDate);
    const endDate = DateHelper.getInstance().getEndOfDayInMillis();
    console.log("End date => ", endDate);
    let condition = {
      customer_id: customer_id,
      transaction_at: { [Op.between]: [startDate, endDate] }
    }
    const { limit, offset } = this.paginationHelper.getPagination(pageNumber, pageSize);
    let data = await PurchaseTransactionDefineModel.getInstance().defineModel.findAndCountAll({
      where: condition,
      order: [
        ['id', 'DESC'],
      ]
    })
    console.log("Result => ", data);
    let response = this.paginationHelper.getPagingData(data, pageNumber, limit)
    return response;
  }
}