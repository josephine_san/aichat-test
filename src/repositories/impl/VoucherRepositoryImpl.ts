import { injectable } from "inversify";
import { PaginationHelper } from "../../helpers/PaginationHelper";
import { VoucherDefineModel, VoucherModel } from "../../models/VoucherModel";
import { VoucherRepository } from "../VoucherRepository";

@injectable()
export class VoucherRepositoryImpl implements VoucherRepository {

  paginationHelper: PaginationHelper
  constructor() {
    this.paginationHelper = new PaginationHelper()
  }

  createVoucher(_model: VoucherModel): Promise<VoucherModel> {
    return VoucherDefineModel.getInstance().defineModel.findOrCreate({
      where: {
        code: _model.code
      },
      defaults: _model

    }).then(result => {
      const model = result[0], created = result[1];
      if (!created) {
        throw new Error('voucher code already exists');
      }
      return model;
    });
  }

  async getAllVoucher(pageNumber: number, pageSize: number): Promise<any> {
    let condition = {
      isLock: false,
    }
    const { limit, offset } = this.paginationHelper.getPagination(pageNumber, pageSize);
    let data = await VoucherDefineModel.getInstance().defineModel.findAndCountAll({
      where: condition,
      order: [
        ['id', 'ASC'],
      ]
    })
    // console.log("Result => ", data);
    let response = this.paginationHelper.getPagingData(data, pageNumber, limit)
    return response;
  }

  updateVoucher(_id: number, _model: VoucherModel): Promise<VoucherModel> {
    return VoucherDefineModel.getInstance().defineModel.update({
      code: _model.code,
      isLock: _model.isLock,
    }, {
      where: {
        id: _id
      }
    });
  }

  getVoucherById(_id: number): Promise<VoucherModel>{
    return VoucherDefineModel.getInstance().defineModel.findOne({
      where: {
        id: _id
      }
    })
  }
}