import { injectable } from "inversify";
import { CustomerDefineModel, CustomerModel } from "../../models/CustomerModel";
import { CustomerRepository } from "../CustomerRepository";

@injectable()
export class CustomerRepositoryImpl implements CustomerRepository {

  constructor() {
  }

  getCustomerByPhone(_phone: string): Promise<CustomerModel> {
    return CustomerDefineModel.getInstance().defineModel.findOne({
      where: {
        contact_number: _phone
      }
    })
  }
}