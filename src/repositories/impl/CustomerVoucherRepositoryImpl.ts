import { injectable } from "inversify";
import { CustomerVoucherDefineModel, CustomerVoucherModel } from "../../models/CustomerVoucherModel";
import { PaginationHelper } from "../../helpers/PaginationHelper";
import { VoucherDefineModel } from "../../models/VoucherModel";
import { CustomerVoucherRepository } from "../CustomerVoucherRepository";
import { Op } from "sequelize";

@injectable()
export class CustomerVoucherRepositoryImpl implements CustomerVoucherRepository {

  paginationHelper: PaginationHelper
  constructor() {
    this.paginationHelper = new PaginationHelper()
  }

  createCustomerVoucherModel(_model: CustomerVoucherModel): Promise<CustomerVoucherModel> {
    return CustomerVoucherDefineModel.getInstance().defineModel.findOrCreate({
      where: {
        customerId: _model.customerId
      },
      defaults: _model

    }).then(result => {
      const model = result[0], created = result[1];
      if (!created) {
        throw new Error('Customer has been already use voucher');
      }
      return model;
    });
  }

  getCustomerVoucherByCustomerId(_customerId: number): Promise<CustomerVoucherModel> {
    return CustomerVoucherDefineModel.getInstance().defineModel.findOne({
      where: {
        customerId: _customerId
      }
    })
  }

  getCustomerVoucherWithinDate(_customerId: number, _date: number): Promise<CustomerVoucherModel> {
    let condition = {
      customerId: _customerId,
      startDate: { [Op.lte]: _date },
      endDate: {[Op.gte]: _date}
    }
    return CustomerVoucherDefineModel.getInstance().defineModel.findOne({
      where: condition
    })
  }

  updatedCustomerVoucher(_id: number, _model: CustomerVoucherModel): Promise<CustomerVoucherModel> {
    return VoucherDefineModel.getInstance().defineModel.update({
      isValid: _model.isValid,
      isUsed: _model.isUsed
    }, {
      where: {
        id: _id
      }
    });
  }

  destroy(customerId: number, voucherId: number): Promise<CustomerVoucherModel> {
    return CustomerVoucherDefineModel.getInstance().defineModel.destroy({
      where: {
        customerId: customerId,
        voucherId: voucherId
      }
    })
  }
}