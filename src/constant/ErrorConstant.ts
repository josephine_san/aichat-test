enum ErrorConstant {
	INTERNAL_SERVICE_ERROR_500 = '500|Internal Service Error',
	NOT_FOUND_404 = '404|Data not found',
	LESS_TRANSCTION_TOTAL_422 = '422|Transaction Complete Minimimum Not Achieve',
	VOUCHER_LIMIT_REACH_422 = '422|Voucher Limit Reach',
	NOT_ELIGIBLE_422 = '422|Customer Not Eligible'
}

export default ErrorConstant;
