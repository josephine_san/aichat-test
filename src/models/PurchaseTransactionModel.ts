import { Model, BuildOptions, Sequelize, DataTypes, UUIDV4 } from 'sequelize';

export interface PurchaseTransactionModel {
  id: number;
  customer_id: number;
  total_spent: number;
  total_saving: number;
  transaction_at: string;
  created_at: string;
  updated_at: string;
}

interface PurchaseTransactionSequelizeModel extends PurchaseTransactionModel, Model {

}

type PurchaseTransactionModelStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): PurchaseTransactionSequelizeModel;
}

export class PurchaseTransactionDefineModel {
  private static instance: PurchaseTransactionDefineModel;
  defineModel: PurchaseTransactionModelStatic;

  private constructor(sequelize: Sequelize) {
    this.defineModel = <PurchaseTransactionModelStatic>sequelize.define('purchase_transactions', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      customer_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      total_spent: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false
      },
      total_saving: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false
      },
      transaction_at: {
        type: new DataTypes.DATE,
        allowNull: true,
      },
      createdAt: {
        type: new DataTypes.DATE,
        allowNull: true,
      },
      updatedAt: {
        type: new DataTypes.DATE,
        allowNull: true,
      },
    });
  }

  static getInstance(sequelize?: Sequelize): PurchaseTransactionDefineModel {
    if (PurchaseTransactionDefineModel.instance === null || PurchaseTransactionDefineModel.instance === undefined) {
      if (sequelize === null || sequelize === undefined) {
        throw new Error('Please initiate the define model class with sequelize first!');
      } else {
        PurchaseTransactionDefineModel.instance = new PurchaseTransactionDefineModel(sequelize);
      }
    }
    return PurchaseTransactionDefineModel.instance;
  }
}