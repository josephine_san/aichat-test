import { Model, BuildOptions, Sequelize, DataTypes, UUIDV4 } from 'sequelize';

export interface CustomerVoucherModel {
  id?: number;
  voucherId?: number;
  customerId?: number;
  isValid?: boolean;
  startDate?: number;
  endDate?: number;
  created_at?: string;
  updated_at?: string;
  isUsed?: boolean;
}

interface CustomerVoucherSequelizeModel extends CustomerVoucherModel, Model {

}

type CustomerVoucherModelStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): CustomerVoucherSequelizeModel;
}

export class CustomerVoucherDefineModel {
  private static instance: CustomerVoucherDefineModel;
  defineModel: CustomerVoucherModelStatic;

  private constructor(sequelize: Sequelize) {
    this.defineModel = <CustomerVoucherModelStatic>sequelize.define('customer_vouchers', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      voucherId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      customerId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      isValid: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      startDate: {
        type: new DataTypes.DATE,
        allowNull: false,
      },
      endDate: {
        type: new DataTypes.DATE,
        allowNull: false,
      },
      createdAt: {
        type: new DataTypes.DATE,
        allowNull: true,
      },
      updatedAt: {
        type: new DataTypes.DATE,
        allowNull: true,
      },
      isUsed: {
        type: DataTypes.BOOLEAN,
        allowNull: true
      }
    });
  }

  static getInstance(sequelize?: Sequelize): CustomerVoucherDefineModel {
    if (CustomerVoucherDefineModel.instance === null || CustomerVoucherDefineModel.instance === undefined) {
      if (sequelize === null || sequelize === undefined) {
        throw new Error('Please initiate the define model class with sequelize first!');
      } else {
        CustomerVoucherDefineModel.instance = new CustomerVoucherDefineModel(sequelize);
      }
    }
    return CustomerVoucherDefineModel.instance;
  }
}