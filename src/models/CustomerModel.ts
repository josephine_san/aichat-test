import {Model, BuildOptions, Sequelize, DataTypes, UUIDV4} from 'sequelize';

export interface CustomerModel {
    id: number;
    first_name: string;
    last_name: string;
    gender: string;
    date_of_birth: Date;
    contact_number: string;
    email: string;
    created_at: string;
    updated_at: string;
}

interface CustomerSequelizeModel extends CustomerModel, Model {

}

type CustomerModelStatic = typeof Model & {
    new (values?: object, options?: BuildOptions): CustomerSequelizeModel;
}

export class CustomerDefineModel {
    private static instance: CustomerDefineModel;
    defineModel: CustomerModelStatic;

    private constructor(sequelize: Sequelize) {
        this.defineModel = <CustomerModelStatic>sequelize.define('customers', {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            first_name: {
                type: new DataTypes.STRING(255),
                allowNull: false,
            },
            last_name: {
                type: new DataTypes.STRING(255),
                allowNull: false,
            },
            gender: {
                type: new DataTypes.STRING(255),
                allowNull: true,
            },
            date_of_birth: {
                type: new DataTypes.DATE,
                allowNull: true,
            },
            contact_number: {
                type: new DataTypes.STRING(255),
                allowNull: false,
            },
            email: {
                type: new DataTypes.STRING(255),
                allowNull: false,
            },
            createdAt: {
                type: new DataTypes.DATE,
                allowNull: true,
            },
            updatedAt: {
                type: new DataTypes.DATE,
                allowNull: true,
            },
        });
    }

    static getInstance(sequelize?: Sequelize): CustomerDefineModel {
        if (CustomerDefineModel.instance === null || CustomerDefineModel.instance === undefined) {
            if (sequelize === null || sequelize === undefined ) {
                throw new Error('Please initiate the define model class with sequelize first!');
            } else {
                CustomerDefineModel.instance = new CustomerDefineModel(sequelize);
            }
        }
        return CustomerDefineModel.instance;
    }
}