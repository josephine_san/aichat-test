import { Model, BuildOptions, Sequelize, DataTypes } from 'sequelize';

export interface VoucherModel {
  id?: number;
  code?: string;
  isLock?: boolean;
  created_at?: string;
  updated_at?: string;
}

interface VoucherSequelizeModel extends VoucherModel, Model {

}

type VoucherModelStatic = typeof Model & {
  new(values?: object, options?: BuildOptions): VoucherSequelizeModel;
}

export class VoucherDefineModel {
  private static instance: VoucherDefineModel;
  defineModel: VoucherModelStatic;

  private constructor(sequelize: Sequelize) {
    this.defineModel = <VoucherModelStatic>sequelize.define('vouchers', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: new DataTypes.STRING(255),
        allowNull: false,
      },
      createdAt: {
        type: new DataTypes.DATE,
        allowNull: true,
      },
      updatedAt: {
        type: new DataTypes.DATE,
        allowNull: true,
      },
      isLock: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
    });
  }

  static getInstance(sequelize?: Sequelize): VoucherDefineModel {
    if (VoucherDefineModel.instance === null || VoucherDefineModel.instance === undefined) {
      if (sequelize === null || sequelize === undefined) {
        throw new Error('Please initiate the define model class with sequelize first!');
      } else {
        VoucherDefineModel.instance = new VoucherDefineModel(sequelize);
      }
    }
    return VoucherDefineModel.instance;
  }
}